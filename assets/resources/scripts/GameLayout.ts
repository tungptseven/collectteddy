// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator

export enum GameStatus {
    Game_Ready = 0,
    Game_Playing,
    Game_Over
}

@ccclass
export default class GameLayout extends cc.Component {
    gameStatus: GameStatus = GameStatus.Game_Ready
    gameOverLayout = null
    toys: cc.Node[] = []
    gameScore: number = 0
    time: number = 61
    toyList = []

    @property(cc.Prefab)
    toy1: cc.Prefab = null
    @property(cc.Prefab)
    toy2: cc.Prefab = null
    @property(cc.Prefab)
    toy3: cc.Prefab = null
    @property(cc.Prefab)
    toy4: cc.Prefab = null
    @property(cc.Prefab)
    toy5: cc.Prefab = null
    @property(cc.Prefab)
    toy6: cc.Prefab = null
    @property(cc.Prefab)
    toy7: cc.Prefab = null
    @property(cc.Prefab)
    toy8: cc.Prefab = null
    @property(cc.Prefab)
    toy9: cc.Prefab = null

    @property(cc.Label)
    lblScore: cc.Label = null

    @property(cc.Label)
    lblTime: cc.Label = null

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        var collisionManager = cc.director.getCollisionManager()
        collisionManager.enabled = true
        this.toyList = [this.toy1, this.toy2, this.toy3, this.toy4, this.toy5, this.toy6, this.toy7, this.toy8, this.toy9]
        this.gameScore = 0
        this.time = 61
        this.lblScore.string = this.gameScore.toString()
        this.lblTime.string = this.time.toString()

        this.gameOverLayout = cc.Canvas.instance.node.getChildByName('GameOverLayout')

    }

    onSpawn(index?: number) {
        let x = -500
        if (index) {
            let i = index - 1
            let toy = cc.instantiate(this.toyList[i])
            toy.x = -500 + (120 * i)
            toy.y = -100
            setTimeout(() => this.node.getChildByName('Toys').addChild(toy), 1000)
        } else {
            for (let i = 0;i < this.toyList.length;i++) {
                let toy = cc.instantiate(this.toyList[i])
                toy.x = x
                toy.y = -100

                this.node.getChildByName('Toys').addChild(toy)
                this.toys.push(toy)
                x += 120
            }
        }
    }

    onSpawnSingle(toy: any) {
        switch (toy.name) {
            case 'toy1': {
                this.onSpawn(1)
                break
            }
            case 'toy2': {
                this.onSpawn(2)
                break
            }
            case 'toy3': {
                this.onSpawn(3)
                break
            }
            case 'toy4': {
                this.onSpawn(4)
                break
            }
            case 'toy5': {
                this.onSpawn(5)
                break
            }
            case 'toy6': {
                this.onSpawn(6)
                break
            }
            case 'toy7': {
                this.onSpawn(7)
                break
            }
            case 'toy8': {
                this.onSpawn(8)
                break
            }
            case 'toy9': {
                this.onSpawn(9)
                break
            }
        }
    }

    onPlay(isReplay: boolean = true) {
        this.gameStatus = GameStatus.Game_Playing
        this.gameScore = 0
        this.lblScore.string = this.gameScore.toString()
        this.onSpawn()
        this.countdown(this.time)
    }

    gameOver() {
        this.gameStatus = GameStatus.Game_Over
        cc.Canvas.instance.node.getChildByName('GameLayout').active = false
        this.gameOverLayout.active = true
        this.gameOverLayout.getComponent('GameOverLayout').gameScore = this.gameScore
        this.node.getChildByName('Toys').destroyAllChildren()
    }

    countdown(time: number) {
        --time
        this.lblTime.string = time.toString()
        if (time > 0) {
            this.scheduleOnce(() => this.countdown(time), 1)
        } else {
            this.gameOver()
        }
        this.lblTime.string = time.toString()
    }

    // update(dt) { }
}
