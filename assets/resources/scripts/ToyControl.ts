// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import GameLayout from "./GameLayout"

const { ccclass, property } = cc._decorator

@ccclass
export default class ToyControl extends cc.Component {
    gameLayout: GameLayout = null

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        this.gameLayout = cc.Canvas.instance.node.getChildByName('GameLayout').getComponent('GameLayout')
    }

    onCollisionEnter(other, self) {
        this.playDeath()
    }

    onRemove() {
        this.node.removeFromParent()
        this.node.destroy()
        // this.gameLayout.onSpawnSingle(this.node)
    }

    playDeath() {
        this.node.runAction(cc.scaleTo(0, 1.2))
        this.node.getComponent(cc.CircleCollider).enabled = false
        this.node.runAction(cc.moveTo(3, cc.v2(this.gameLayout.node.getChildByName('Magnet').position.x, 221)))
        this.scheduleOnce(() => this.onRemove(), 3)
    }

    start() {
        // this.onSpawn()
    }

    // update (dt) {}
}
