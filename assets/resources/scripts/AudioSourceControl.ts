// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator

export enum SoundType {
    E_Sound_Down = 0,
    E_Sound_Catch,
    E_Sound_Background
}
@ccclass
export default class AudioSourceControl extends cc.Component {

    @property({ type: cc.AudioClip })
    backgroundMusic: cc.AudioClip = null

    // sound effect when bird flying
    @property({ type: cc.AudioClip })
    downSound: cc.AudioClip = null

    @property({ type: cc.AudioClip })
    catchSound: cc.AudioClip = null


    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    onStart(isMute: boolean) {
        if (!isMute) {
            cc.audioEngine.playMusic(this.backgroundMusic, true)
        } else { return }
    }

    playSound(type: SoundType) {
        if (type == SoundType.E_Sound_Down) {
            cc.audioEngine.playEffect(this.downSound, false)
        }
        else if (type == SoundType.E_Sound_Catch) {
            cc.audioEngine.playEffect(this.catchSound, false)
        }
    }

    pause() {
        cc.audioEngine.pauseMusic()
    }

    resume() {
        cc.audioEngine.resumeMusic()
    }
    // update (dt) {}
}
