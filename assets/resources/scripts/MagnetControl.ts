// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import GameLayout from "./GameLayout"
import GameScene from "./GameScene"
import { SoundType } from "./AudioSourceControl"

const { ccclass, property } = cc._decorator

@ccclass
export default class MagnetControl extends cc.Component {
    gameControl: GameScene = null
    isDownDirection: boolean = false
    gameLayout: GameLayout = null
    isAvailable: boolean = true
    clickTime: any = new Date().getTime()

    // LIFE-CYCLE CALLBACKS:
    onReset() {
        this.node.x = 221
        this.node.y = 500
        this.isAvailable = true
    }

    onMoveX() {
        let toLeft = cc.moveTo(5, cc.v2(-500, this.node.y))
        let toRight = cc.moveTo(5, cc.v2(500, this.node.y))
        return (cc.sequence(toLeft, toRight))
    }

    onCatch() {
        this.gameControl.audioSourceControl.playSound(SoundType.E_Sound_Down)
        let curX = this.node.x
        this.node.stopAllActions()
        this.node.runAction(cc.sequence(
            cc.moveTo(2, cc.v2(this.node.x, -60)),
            this.onUp(curX)
        ))

        setTimeout(() => this.node.runAction(cc.repeatForever(this.onMoveX())), 5000)
    }

    onCollisionEnter(other, self) {
        if (other.tag === 1) {
            this.gameLayout.gameScore += 50
            this.gameLayout.lblScore.string = this.gameLayout.gameScore.toString()
            this.gameControl.audioSourceControl.playSound(SoundType.E_Sound_Catch)
        }
    }

    onUp(currentX) {
        return (cc.moveTo(3, cc.v2(currentX, 200)))
    }

    onLoad() {
        cc.Canvas.instance.node.on(cc.Node.EventType.TOUCH_START, this.onTouchStart, this)
        this.node.runAction(cc.repeatForever(this.onMoveX()))
        this.gameLayout = cc.Canvas.instance.node.getChildByName('GameLayout').getComponent('GameLayout')
        this.gameControl = cc.Canvas.instance.node.getComponent('GameScene')
    }

    start() {

    }

    checkClick(firstClick, lastClick) {
        if (this.isAvailable) {
            if (lastClick - firstClick >= 600) {
                this.isAvailable = false
                let openClick = cc.callFunc(() => {
                    setTimeout(() => this.isAvailable = true, 6000)
                }, this)
                this.node.runAction(cc.sequence(cc.callFunc(() => this.onCatch()), openClick))
            } else {
                return
            }
        } else { return }
    }

    onTouchStart() {
        let curClick = new Date().getTime()
        this.checkClick(this.clickTime, curClick)
        this.clickTime = curClick
    }


    // update(dt) { }
}
