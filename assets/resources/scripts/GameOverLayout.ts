// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html


const { ccclass, property } = cc._decorator

@ccclass
export default class GameOverLayout extends cc.Component {
    gameScore: number = 0

    @property(cc.Label)
    lblScore: cc.Label = null


    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    update(dt) {
        this.lblScore.string = this.gameScore.toString()
    }
}
